import './App.css';
import {BrowserRouter,Route,Routes } from 'react-router-dom'
import Navbar from './Components/Navbar'
import Homepage from './Components/Homepage'
import MyEvent from './Components/MyEvent'
import AboutUs from './Components/AboutUs'
import ContactUs from './Components/ContactUs'


function App() {
  return (
    <div className="App">
          <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path="/" element={<Homepage />}></Route>
        <Route path="/MyEvent" element={<MyEvent />}></Route>
        <Route path="/AboutUs" element={<AboutUs />}></Route>
        <Route path="/ContactUs" element={<ContactUs />}></Route>
        

        
      </Routes>

  
    </BrowserRouter>
    </div>
  );
}

export default App;
