import React from 'react'
import Footer from '../Components/Footer'

function AboutUs() {
  return (
    <div>
        <div>
        <center>
        <img src="https://www.researchgate.net/profile/Rajib-Shaw/publication/237380391/figure/fig2/AS:504716129386496@1497345110133/Schematic-diagram-showing-the-role-of-NGOs-in-multi---stakeholder-cooperation-Central_Q320.jpg" style={{width:'600px',height:'300px'}}  alt=""/>
        </center>
        <p style={{textAlign:'center',justifyContent:'center',fontFamily:'serif'}}>
Event planning can be stressful for any organization, and especially for nonprofits who are usually tight on funds, the success of an event is additionally required. For NGOs, wasting funds will not be an option because doing so will cause many donors and supporters to lose trust in the organization and eventually discontinue the support they provide. For this reason, NGOs must be creative and innovative to ensure present and future triumphs. Raising funds for nonprofits given that it is a necessary task for NGOs, should effect into additional revenue and not costs.

To encourage more attendees to attend your event, make sure your event will be worthwhile, both for their time and the fees they are required to pay. To do so, take note of the following tips which can enable you to host events successfully.</p>
        </div>

        <div>
        <center>
        <img src="https://www.globalexperiences.com/hs-fs/hubfs/images/Banner/Career_Fields/ngo.jpg?width=960&name=ngo.jpg" style={{width:'600px',height:'300px'}}  alt=""/>
        </center>
        <p style={{textAlign:'center',justifyContent:'center',fontFamily:'serif'}}>Hosting events especially for the purpose of fundraising can bring many benefits to NGOs. On the other hand, failure to succeed will likewise not be acceptable. Thus, it is essentially necessary that protocol in hosting events are followed. The examples stated in this article can serve as a guide to proper event management which can bring many opportunities for growth</p>
        </div>
        
<Footer />

    </div>
  )
}

export default AboutUs