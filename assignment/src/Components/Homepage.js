import React from 'react'
import Slides from './Slides'
import Footer from './Footer'

function Homepage(){
    const custumcarousalstyles = { 
        margin:' 0px auto',
        padding:'140px',
        height:'50vh',
        width:'100vw', 
    }
  return (
    <div>
   
   <img src="https://fundsforngosmedia.s3.amazonaws.com/wp-content/uploads/2016/07/28052033/ngo-610x273.png" style={{padding:'30px',width:'1480px'}} alt=""></img>
      <div style={custumcarousalstyles}>
      
        <h1 style={{fontFamily:'fantasy',color:'blue',textAlign:'center'}}>NGO's Event Creation</h1>
        
        <Slides />

        <Footer />
    </div>

    </div>
  )
}

export default Homepage;