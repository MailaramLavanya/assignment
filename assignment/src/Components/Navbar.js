import React from 'react'
import {Link} from 'react-router-dom'
import Logo from '../Images/Logo.jpeg'


function Navbar(){
  return (
    <div className='sticky-top'>
      <nav className="navbar navbar-expand-lg bg-dark">
  <div className="container-fluid">
  <img src={Logo}  style={{width:80}} alt=""/>
    <h1 style={{fontFamily:'cursive',color:'blue'}}>NGO's Event Creation</h1>
    <button className="navbar-toggler bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarNavDropdown">
      <ul className="navbar-nav ml-auto">
        <li className="nav-item">
       <Link to="/" className='text-white nav-link'>Homepage</Link>
        </li>
        <li className="nav-item">
        <Link to="/MyEvent" className='text-white nav-link'>MyEvent</Link>
        </li>
        <li className="nav-item">
        <Link to="/AboutUs" className='text-white nav-link'>AboutUs</Link>
        </li>
        <li className="nav-item">
        <Link to="/ContactUs" className='text-white nav-link'>ContactUs</Link>

        </li>
        
       
      </ul>
    </div>
  </div>
</nav>
    </div>
  )
}

export default Navbar