import React from 'react';
import './Footer.css'; 

const Footer = () => {
  return (
    <footer className="footer">
      <div className="footer-content">
        <p>© 2024 NGO's Event Creation. All rights reserved.</p>
      </div>
    </footer>
  );
};

export default Footer;